@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
super.onActivityResult(requestCode, resultCode, data);
// Result Code is -1 send from Payumoney activity
Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);
if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
if(transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)){
//Success Transaction
} else{
//Failure Transaction
}
// Response from Payumoney
String payuResponse = transactionResponse.getPayuResponse();
// Response from SURl and FURL
String merchantResponse = transactionResponse.getTransactionDetails();
}  else if (resultModel != null && resultModel.getError() != null) {
Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
} else {
Log.d(TAG, "Both objects are null!");
}
}
}