
    public void navigateToBaseActivity(View view){

        merchantkey = " 5BhgWqkQ";   // [ you get this key after Signing up to PayUMoney account ]
        int environment = PayuConstants.STAGING_ENV;  //[ Setting up Test mode ]
        String userEmail = "greaper313@gmail.com";
        String amount = "101";
        userCredentials =  merchantkey + ":" + userEmail;

        mPaymentParams = new PaymentParams();
        mPaymentParams.setKey(merchantkey);
        mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo("Home");
        mPaymentParams.setFirstName("FirstName");
        mPaymentParams.setEmail(userEmail);
        mPaymentParams.setTxnId(""+ System.currentTimeMillis());           // [ Setting up Transaction ID  to system time ]
        mPaymentParams.setSurl("https://payu.herokuapp.com/sucess");      // [ Setting up Successful Transaction link  ]
        mPaymentParams.setFurl("https://payu.herokuapp.com/failure");    // [ Setting up Failure Transaction link  ]
        mPaymentParams.setUdf1("UserDefined 1");            //[ User defined parameters to add more detail for transaction ]
        mPaymentParams.setUdf2("UserDefined 2");
        mPaymentParams.setUdf3("UserDefined 3");
        mPaymentParams.setUdf4("UserDefined 4");
        mPaymentParams.setUdf5("UserDefined 5");
        mPaymentParams.setUserCredentials(userCredentials);

        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(environment);

        generateHashFromServer(mPaymentParams ); // [ here we are calling a hash function by taking mpayment parameters]
    }


    public void generateHashFromServer(PaymentParams mPaymentParams ) {

        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));   //[ call function concatParams to bind values ]
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount())); //[ Bind  payment amount ]
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        if ( mPaymentParams.getOfferKey() != null)
        {
        postParamsBuffer.append(concatParams(PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey()));
        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();
        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
        }


    } // [End of generate Hash function ]

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    private class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes>{
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try{
                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");
                String postParam = postParams[0];  // get the payuConfig first
                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }
                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch(key){

                        case "payment_hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;

                        case "vas_for_mobile_sdk_hash":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;

                        case "payment_related_details_for_mobile_sdk_hash":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;

                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;

                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;

                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;

                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;

                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        default:
                            break;
                    }

                }


            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return payuHashes;
        }


        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);
            
            progressDialog.dismiss();
            Toast.makeText(MainActivity.this, "Click", Toast.LENGTH_SHORT).show();
            launchSdkUI(payuHashes);
        }

    } // [End of GetHashesFromServer ]
        /**
         * This method adds the Payuhashes and other required params to intent and launches the PayuBaseActivity.java
         *
         * @param payuHashes it contains all the hashes generated from merchant server
         */

        public void launchSdkUI(PayuHashes payuHashes) {

            Intent intent = new Intent(this, PayUBaseActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
            intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);

            //Lets fetch all the one click card tokens first           //Intent passing
           startActivityForResult(intent,PayuConstants.PAYU_REQUEST_CODE);
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PayuConstants.PAYU_REQUEST_CODE){

            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage("Pay U Data : " + data.getStringExtra("payu_response") + "\n\n\n Merchant Data :" )
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                        }
                    }).show();

        }
        else {

            Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_SHORT).show();
        }


    }
