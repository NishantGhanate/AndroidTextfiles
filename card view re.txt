<?xml version="1.0" encoding="utf-8"?>

<RelativeLayout xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:padding="20dp"
    xmlns:android="http://schemas.android.com/apk/res/android">

    <android.support.v7.widget.CardView
        android:id="@+id/cardView"
        android:backgroundTint="@color/colorPrimary"
        android:layout_width="match_parent"
        android:layout_height="120dp">

        <android.support.constraint.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="100dp"
            android:orientation="vertical"
            >

            <TextView
                android:id="@+id/textViewSpeed"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="ss"
                android:textSize="20sp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_bias="0.174"
                app:layout_constraintLeft_toLeftOf="parent"

                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintVertical_bias="0.219" />

            <TextView
                android:id="@+id/textViewValid"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="ss"
                android:textSize="20sp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_bias="0.46"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintVertical_bias="0.219" />

            <TextView
                android:id="@+id/textViewPrice"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="bottom"
                android:text="asas"
                android:textAppearance="?android:attr/textAppearanceSmall"
                android:textSize="20sp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintHorizontal_bias="0.801"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintVertical_bias="0.219" />

        </android.support.constraint.ConstraintLayout>
    </android.support.v7.widget.CardView>

</RelativeLayout>